using System;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Net.Http.Headers;
using System.Net.Http;
using ChoETL;

namespace cds_webapi
{
    class Program
    {   

        static void Main(string[] args)
        {  
            // Set values for connection:
            string url = "https://org0910ac0d.api.crm5.dynamics.com";
            string userName = "tiffany@quantr.hk";
            string password = "password";

            // Azure Active Directory registered app clientid for Microsoft samples
            string clientId = "bf80a31b-2732-4a5d-bb17-2c6b2fd24ad5";
            string webApiUrl = $"https://org0910ac0d.api.crm5.dynamics.com/api/data/v9.0/";
            var userCredential = new UserCredential(userName, password);

            //Authenticate using IdentityModel.Clients.ActiveDirectory
            var authParameters = AuthenticationParameters.CreateFromResourceUrlAsync(new Uri(webApiUrl)).Result;
            var authContext = new AuthenticationContext(authParameters.Authority, false);
            var authResult = authContext.AcquireToken(url, clientId, userCredential);
            var authHeader = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);            

            using (var clientjs = new HttpClient())
            {
                clientjs.BaseAddress = new Uri(webApiUrl);
                clientjs.DefaultRequestHeaders.Authorization = authHeader;
                HttpResponseMessage result = clientjs.GetAsync($"{webApiUrl}cr56f_contactlists").Result;
                result.EnsureSuccessStatusCode();
                var response = result.Content.ReadAsStringAsync().Result;  //Get JSON from ODATA service
                //Console.WriteLine(response);

                System.IO.File.WriteAllText("tmp.json",response);
                Console.WriteLine(response);
                using (var csv = new ChoCSVWriter("cdsdata.csv").WithFirstLineHeader())
                {
                    using (var json = new ChoJSONReader("tmp.json")
                        .WithJSONPath("$..value[*]")                      
                    )
                    {
                        csv.Write(json);
                    }
                }
                Console.ReadLine();
            }

        }
    }

}
